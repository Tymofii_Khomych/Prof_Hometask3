﻿using System.IO;
namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using (var file = new FileStream("Task1.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (var writer = new StreamWriter(file))
                {
                    writer.WriteLine("Hello");
                    writer.WriteLine("How u doing?");
                    writer.WriteLine("Whats up?");
                }

                file.Close();

                using (var reader = new StreamReader("Task1.txt"))
                {
                    string content = reader.ReadToEnd();
                    Console.WriteLine(content);
                }
            }
        }
    }
}