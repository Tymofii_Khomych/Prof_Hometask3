﻿using System.IO.Compression;

namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo dir = new DirectoryInfo(@"D:");
            DirectoryInfo[] subdirectories = dir.GetDirectories();

            foreach (var directory in subdirectories)
            {
                Console.WriteLine($"Directory: {directory.Name}");
            }

            var testFile = new FileInfo(@"D:\Test.txt");
            string fileToFind = testFile.FullName;

            Console.WriteLine(new string('-', 20));

            using (FileStream stream = testFile.Create())
            {
                bool found = File.Exists(fileToFind);

                if (found)
                {
                    Console.WriteLine($"File {fileToFind} was found");

                    stream.Close();
                    using (StreamWriter writer = new StreamWriter(@"D:\Test.txt"))
                    {
                        writer.WriteLine("Hello");
                        writer.WriteLine("How u doing?");
                        writer.WriteLine("Whats up?");
                    }

                    Console.WriteLine("\nFile content:");
                    using (var reader = new StreamReader(@"D:\Test.txt"))
                    {
                        string content = reader.ReadToEnd();
                        Console.WriteLine(content);
                    }

                    string filePath = @"D:\Test.txt";
                    string archivePath = @"D:\archive.zip";

                    using (ZipArchive archive = ZipFile.Open(archivePath, ZipArchiveMode.Create))
                    {
                        ZipArchiveEntry entry = archive.CreateEntryFromFile(filePath, Path.GetFileName(filePath));
                    }
                }
                else
                {
                    Console.WriteLine($"File {fileToFind} was not found");
                }
            }
        }
    }
}