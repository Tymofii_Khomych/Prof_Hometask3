﻿using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;

namespace WpfIsolatedStorageExample
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SaveData_Click(object sender, RoutedEventArgs e)
        {
            string data = dataTextBox.Text;

            using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForAssembly())
            {
                using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream("data.txt", FileMode.OpenOrCreate, storage))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        writer.Write(data);
                    }
                }
            }

            MessageBox.Show("Data saved to isolated storage.");
        }

        private void LoadData_Click(object sender, RoutedEventArgs e)
        {
            using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForAssembly())
            {
                if (storage.FileExists("data.txt"))
                {
                    using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream("data.txt", FileMode.OpenOrCreate, storage))
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            string data = reader.ReadToEnd();
                            MessageBox.Show($"Data loaded from isolated storage: {data}");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No data found in isolated storage.");
                }
            }
        }
    }
}
