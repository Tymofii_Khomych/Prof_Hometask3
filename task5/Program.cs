﻿namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Использование Visual C# для выполнения простого операций ввода-вывода файлов
            // https://learn.microsoft.com/ru-ru/troubleshoot/developer/visualstudio/csharp/language-compilers/file-io-operation

            /*
             * Демонстрация операций ввода-вывода файлов
             * В примерах в этой статье описываются основные операции ввода-вывода файлов. В разделе Пошаговый пример описывается создание примера программы, 
             * демонстрирующей следующие операции ввода-вывода файлов:
             *      Чтение текстового файла
             *      Запись текстового файла
             *      Просмотр сведений о файлах
             *      Вывод списка дисков
             *      Список папок
             *      Вывод списка файлов
             *     
             * Чтение текстового файла
             * В следующем примере кода для чтения файлаSystem.ini используется StreamReader класс . 
             * Содержимое файла добавляется в элемент управления ListBox. Блок try...catch используется для оповещения программы, 
             * если файл пуст. Существует множество способов определить, когда достигается конец файла. В этом примере метод используется 
             * Peek для изучения следующей строки перед ее чтением.
             *      
             *      StreamReader reader=new StreamReader(winDir + "\\system.ini");
                    try
                    {
                        do
                        {
                            addListItem(reader.ReadLine());
                        }
                        while(reader.Peek()!= -1);
                    }
                    catch
                    {
                        addListItem("File is empty");
                    }
                    finally
                    {
                        reader.Close();
                    }
            *
            * Запись текстового файла
            * Этот пример кода использует класс для StreamWriter создания и записи в файл. Если у вас есть существующий файл, 
            * его можно открыть таким же образом.
            * 
                    StreamWriter writer = new StreamWriter("c:\\KBTest.txt");
                    writer.WriteLine("File created using StreamWriter class.");
                    writer.Close();
                    this.listbox1.Items.Clear();
                    addListItem("File Written to C:\\KBTest.txt");

            * Просмотр сведений о файлах
            * Этот пример кода использует FileInfo объект для доступа к свойствам файла. в этом примере используется Notepad.exe. 
            * Свойства отображаются в элементе управления ListBox.
                    FileInfo FileProps =new FileInfo(winDir + "\\notepad.exe");
                    addListItem("File Name = " + FileProps.FullName);
                    addListItem("Creation Time = " + FileProps.CreationTime);
                    addListItem("Last Access Time = " + FileProps.LastAccessTime);
                    addListItem("Last Write TIme = " + FileProps.LastWriteTime);
                    addListItem("Size = " + FileProps.Length);
                    FileProps = null;

            * Вывод списка дисков
            * В этом примере кода классы и Drive используются Directory для вывода списка логических дисков в системе. 
            * В этом примере результаты отображаются в элементе управления ListBox.
                string[] drives = Directory.GetLogicalDrives();
                foreach(string drive in drives)
                {
                    addListItem(drive);
                }
            * Вывод списка вложенных папок
                string[] files= Directory.GetFiles(winDir);
                foreach (string i in files)
                {
                    addListItem(i);
                }

            * Вывод списка файлов
                string[] files= Directory.GetFiles(winDir);
                foreach (string i in files)
                {
                    addListItem(i);
                }
             */
        }
    }
}